/* RemoteClient.c
   Se introducen las primitivas necesarias para establecer una conexión simple
   dentro del lenguaje C utilizando sockets.
*/
/* Cabeceras de Sockets */
#include <sys/types.h>
#include <sys/socket.h>
/* Cabecera de direcciones por red */
#include <netdb.h>
/**********/
#include <sys/select.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

/*
  El archivo describe un sencillo cliente que se conecta al servidor establecido
  en el archivo RemoteServer.c. Se utiliza de la siguiente manera:
  $cliente IP port
 */

void error(char *msg){
    exit((perror(msg), 1));
}

int handle_stdin(int sock){
    char input_buf[1024];
    scanf (" %[^\n]", input_buf);
    fflush(stdin);
    getchar();
    send(sock, input_buf, strlen(input_buf)+1, 0);
    if (strcmp (input_buf, "/exit") == 0)
        return 0;
    else
        return 1;

}

void handle_socket(int sock){
    int bytes_read;
    char buffer[2048];
    bytes_read = recv (sock, buffer, sizeof(buffer), 0);
    if (bytes_read < 0){
            perror ("socket_read");
            exit (EXIT_FAILURE);
    }
    buffer[bytes_read] = 0;
    printf ("%s\n", buffer);
}


int sock;
struct addrinfo *resultado;

void close_client (){
    send(sock, "/exit", sizeof("/exit"), 0);
    freeaddrinfo(resultado);
    close(sock);
    exit(EXIT_SUCCESS);
}


int main(int argc, char **argv){
    int nick_not_set = 1, not_exit = 1, bytes_read;
    char buf[2048], input_buf[2048];
    fd_set active_fd_set, read_fd_set;


    /*Chequeamos mínimamente que los argumentos fueron pasados*/
    if(argc != 3){
        fprintf(stderr,"El uso es \'%s IP port\'", argv[0]);
        exit(1);
    }

    /* Inicializamos el socket */
    if( (sock = socket(AF_INET , SOCK_STREAM, 0)) < 0 )
        error("No se pudo iniciar el socket");

    /* Buscamos la dirección del hostname:port */
    if (getaddrinfo(argv[1], argv[2], NULL, &resultado)){
        fprintf(stderr,"No se encontro el host: %s \n",argv[1]);
        exit(2);
    }

    if(connect(sock, (struct sockaddr *) resultado->ai_addr, resultado->ai_addrlen) != 0)
        /* if(connect(sock, (struct sockaddr *) &servidor, sizeof(servidor)) != 0) */
        error("No se pudo conectar :(. ");

    printf("La conexión fue un éxito!\n");
    signal(SIGINT, close_client);

    while (nick_not_set){
        bytes_read = recv(sock, buf, sizeof(buf),0);
        buf[bytes_read] = 0;
        printf("Server:%s \n", buf);
        scanf ("%s",input_buf);
        fflush(stdin);
        getchar();
        send(sock, input_buf, strlen(input_buf)+1, 0);
        bytes_read = recv(sock, buf, sizeof(buf),0);
        buf[bytes_read] = 0;
        if (strcmp(input_buf, "/exit"))
            printf("Server:%s\n", buf);
        else
            not_exit = 0;
        if (strcmp(buf, "Nickname aceptado") == 0)
            nick_not_set = 0;
    }


    FD_ZERO (&active_fd_set);
    FD_SET (sock, &active_fd_set);
    FD_SET(STDIN_FILENO,  &active_fd_set);

    while(not_exit){
        read_fd_set = active_fd_set;
        if (select (sock + 1, &read_fd_set, NULL, NULL, NULL) < 0) {
            perror ("select");
            exit (EXIT_FAILURE);
        }
        if (FD_ISSET(STDIN_FILENO, &read_fd_set)){
            not_exit = handle_stdin(sock);
        }
        if (FD_ISSET(sock, &read_fd_set)){
            handle_socket(sock);
        }
    }
    freeaddrinfo(resultado);
    close(sock);
    
    return 0;
}