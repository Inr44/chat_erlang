FLAGS = -Wall -Wextra -Wpedantic -g

client: client.c
	gcc $(FLAGS) client.c -o client -pthread
