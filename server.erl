-module(server).
-export([start/1, receptor/1, broker/1, peer/1, receiver/2]).

start(Puerto)->
    Broker = spawn(?MODULE, broker,[maps:new()]),
    register(broker_n, Broker),
    %% register(servidor, self()),
    {ok, Socket} = gen_tcp:listen(Puerto
                                 , [ binary, {active, false}]),
    spawn(?MODULE,receptor, [Socket]),
    Socket.


receptor(Socket) ->
    case gen_tcp:accept(Socket) of
        {ok, CSocket}  ->
            spawn(?MODULE, peer,[CSocket]);
        {error, closed} ->
            io:format("Se cerró el Socket, nos vimos"),
            exit(normal);
        {error, Reason} ->
            io:format("Falló la espera del client por: ~p~n",[Reason])
    end,
    receptor(Socket).

check_nickname(Paquete) ->
    if
        Paquete == <<"/exit\0">> ->
            quit;
        true -> 
            broker_n ! {reg, self(), lists:droplast(binary_to_list(Paquete))},
            receive
                invalid -> 
                    invalid;
                {valid, Nick} -> 
                    {valid, Nick}
            end   
    end.

peer(Socket) ->
    gen_tcp:send(Socket, list_to_binary("Ingrese su nickname\0")),
    case gen_tcp:recv(Socket, 0) of
        {ok, Paquete} ->
            io:format("Peer ~p: Me llegó ~p ~n",[self(),Paquete]),
            case check_nickname(Paquete) of
                quit ->
                    ok;
                {valid, Nickname} ->
                    gen_tcp:send(Socket, list_to_binary("Nickname aceptado\0")),
                    spawn(?MODULE, receiver, [Socket, self()]), 
                    peer(Socket, Nickname);
                invalid ->
                    gen_tcp:send(Socket, list_to_binary("Nickname en uso\0")),
                    peer(Socket)
            end;
        {error, closed} ->
            io:format("El cliente cerró la conexión~n")
    end.

process_msg(Socket, Peer, Paquete) ->
    case binary_to_list(Paquete) of
        "/exit\0" ->
            Peer ! quit;
        "/msg " ++ Package ->
            [Dest, Msg] = string:split(Package, " "),
            io:format("Peer ~p: Procese el whisper ~s  para ~s ~n",[Peer,Msg, Dest]),
            Peer ! {priv, Msg, Dest},
            receiver(Socket, Peer);
        "/nickname " ++ New_nick ->
            io:format("Peer ~p: Procese el cambio de nick a ~s ~n",[Peer,New_nick]),
            Peer ! {change, lists:droplast(New_nick)},
            receiver(Socket, Peer);
        Msg ->
            io:format("Peer ~p: Procese el msg ~s ~n",[Peer,Msg]),
            Peer ! {general, Msg},
            receiver(Socket, Peer)
    end.

receiver(Socket, Peer) ->
    case gen_tcp:recv(Socket, 0) of
        {ok, Paquete} ->
            io:format("Peer ~p: Me llegó ~p ~n",[Peer,Paquete]),
            process_msg(Socket, Peer, Paquete);
        {error, closed} ->
            io:format("El cliente cerró la conexión~n")
    end.

generate_priv(Paquete, Nickname, Dest) ->
    Nickname ++ " a " ++ Dest ++ ": " ++ Paquete.

peer(Socket, Nickname) ->
    receive
        %Receiver messages
        quit ->
            broker_n ! {quit, self(), Nickname},
            io:format("Peer ~p: Signing out ~n",[self()]),
            ok;
        {general, Msg} -> 
            io:format("Peer ~p: Voy a enviar el mensaje ~s ~n",[self(),Nickname ++ ": " ++ Msg]),
            broker_n ! {env, self(),Nickname ++ ": " ++ Msg},
            peer(Socket, Nickname);
        {priv, Paquete, Dest} ->
            broker_n ! {priv, self(), generate_priv(Paquete, Nickname, Dest), Dest},
            peer(Socket, Nickname);
        {change, New_nick} ->
            io:format("Peer ~p: Voy a pedirle el broker un cambio de nick ~n",[self()]),
            broker_n ! {change, self(), Nickname, New_nick},
            peer(Socket, Nickname);
        %Broker messages
        {msg, Msg} ->
            gen_tcp:send(Socket, Msg),
            peer(Socket, Nickname);
        {new_nick, New_nick} ->
            io:format("Peer ~p: Recibi la orden del broker de cambiar mi nick a ~s ~n",[self(),New_nick]),
            peer(Socket, New_nick);
        invalid_nick ->
            gen_tcp:send(Socket, list_to_binary("Server: Nickname en uso, no se efectuo cambio\0")),
            peer(Socket, Nickname)
    end.

broker(Peer_map) ->
    receive
        {fin, Pid } -> Pid ! servOk, ok;
        {reg, Pid, Nick} ->
            broker(update_map(Peer_map, Nick, Pid));
        {env, _, Msg} ->
            lists:foreach( fun (X) -> X ! {msg, Msg} end , maps:values(Peer_map)),
            broker(Peer_map);
        {priv, Pid, Msg, Dest} -> 
            Pid ! {msg, Msg},
            io:format("Broker: Voy a intentar enviar el whisper ~s para ~s ~n",[Msg, Dest]),
            send_private_msg(Peer_map, Msg, Dest),
            broker(Peer_map);
        {change, Pid, Old_nick, New_nick} ->
            broker(change_nickname(Peer_map, Old_nick, New_nick, Pid));  %change nickname envia confirmacion y nuevo nickname
        {quit, _, Nick} ->
            io:format("Broker: Saque a ~s del map ~n",[Nick]),
            Msg = "Server: El usuario " ++ Nick ++ " ha abandonado la sala \0",
            lists:foreach( fun (X) -> X ! {msg, Msg} end , maps:values(Peer_map)),
            io:format("Broker: Envie el msg '~s' ~n",[Msg]),
            broker(maps:remove(Nick, Peer_map))
    end.


send_private_msg(Peer_map, Msg, Dest_nick) ->
    Flag = maps:is_key(Dest_nick, Peer_map),
    if
        Flag ->
            Dest = maps:get(Dest_nick, Peer_map),
            Dest ! {msg, Msg},
            io:format("Broker: Procese el whisper ~s para ~p (~s) ~n",[Msg,Dest,Dest_nick]),
            ok;
        true ->
            not_found    
    end.

update_map(Peer_map, Nick, Pid) ->
    Map_size = map_size(Peer_map),
    if
        Map_size > 0 ->
            Flag = not maps:is_key(Nick, Peer_map),
            if
                Flag ->
                    Pid ! {valid, Nick},
                    Msg = "Server: El usuario " ++ Nick ++ " se ha unido\0",
                    broker_n ! {env, self(), Msg},
                    maps:put(Nick, Pid, Peer_map);
                true ->
                    Pid ! invalid,
                    Peer_map
            end;
        Map_size == 0 ->
            Pid ! {valid, Nick},
            Msg = "Server: El usuario " ++ Nick ++ " se ha unido\0",
            broker_n ! {env, self(), Msg},
            maps:put(Nick, Pid, Peer_map)
    end.

change_nickname(Peer_map, Old_nick, New_nick, Pid) ->
    Flag = not maps:is_key(New_nick, Peer_map),
    if
        Flag ->
            Pid ! {new_nick, New_nick},
            io:format("Broker:Cambie el nick de '~s' a '~s'  ~n",[Old_nick, New_nick]),
            Msg = "Server: El usuario " ++ Old_nick ++ " ha cambiado su nombre a " ++ New_nick ++ "\0",
            lists:foreach( fun (X) -> X ! {msg, Msg} end , maps:values(Peer_map)),
            maps:put(New_nick, Pid, maps:remove(Old_nick, Peer_map));
        true ->
            Pid ! invalid_nick,
            Peer_map
    end.

